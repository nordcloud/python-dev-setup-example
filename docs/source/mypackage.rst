.. README
   Private and special members are not automaticly added to the documentation.
   This is the recommened way, as users of a framework should not interact with private fields/methods
   To add them you need to explicitly specify the option, see below.


=================
mypackage
=================

Some text can be placed here to explain what this package does.

main
==========

.. automodule:: mypackage.main
   :members:
   :private-members:

foo
=======
.. automodule:: mypackage.foo
   :members:
   :special-members:
