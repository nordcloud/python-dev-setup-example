import unittest

import pytest

from mypackage.main import cloud_ingest


# Pytest example
@pytest.fixture
def input_value():
    input = 39
    return input


def test_answer(input_value):
    assert cloud_ingest(input_value) == 40


# Unittest example
class TestMain(unittest.TestCase):
    def setUp(self):
        self.input = 39

    def test_0010_add(self):
        res = cloud_ingest(self.input)
        self.assertEqual(res, 40)
