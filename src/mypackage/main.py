import logging

import boto3

#: This is the s3 client
s3_client = boto3.client("s3", region_name="eu-west-1")

#: Test
foo = "bar"

print("Hello there!")


def asd(a: int) -> int:
    """Testing method, whats the meaning of life?."""
    logging.info("test")
    return 42


def cloud_ingest(x):
    """Ingests data into the cloud."""
    return x + 1


def _private_method():
    """Private methods are not automaticly added to the documentation"""
    print("private")
