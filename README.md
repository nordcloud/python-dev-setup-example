## Python development environment

### Requirements

* Pipenv https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv
* Python 3.8 (if you did't install Pipenv with Homebrew on MacOS)

### Setup

For initial setup of development environment, run the following command in project root:
```
make setup-dev
```

After the initial setup, and every time you want to activate the Python environment in your terminal:
```
pipenv shell
```

Best practices guide on how to use Pipenv to manage your Python environment: https://realpython.com/pipenv-guide/

### Pre-commit hooks

The initial setup command will install Git pre-commit hooks that will be run automatically on each commit to check code quality.

To run pre-commit hooks manually (only for staged files):
```
pre-commit run
```

For all files:
```
pre-commit run --all-files
```

### Python code locations

Python modules are located in `src` folder.

### Tests

Run tests:
```
pipenv run test
```

Run code coverage:
```
pipenv run test:cov
```

### Adding new dependencies

To add new Python production dependencies (i.e. libraries used by your application code):

```
pipenv install <package_name>[==<version>]
```

For development dependencies (e.g. libraries for testing):

```
pipenv install -d <package_name>[==<version>]
```

## Documentation - Sphinx


### Config


Inside `./docs/conf.py` you can find the configuration for sphinx. 

The values that should be changed for a project are:

(ExampleField = "Recommended value")

```

project = "Project Name"

copyright = "Project/Company"

author = "Team name"

release = "Update this for the current release example: 1.0"

```


Extra options can be added or other values can be changed to the liking of the team.


### Adding new modules:


1) Create a new `.rst` file inside the `./docs/source` folder. Then add the fields needed to autodoc, see `./docs/source/mypackage.rst` for some examples. (more exaples here: www.sphinx-doc.org/en/master/usage/extensions/autodoc.html).

2) Add the name of the new file to `./docs/source/index.rst`


### Extending an existing module


Open the file for the module then add the new classes/methods etc in there.


### Generating the documentation


1) To generate the documentation run with the following commands from the root of the project:

``` 

cd docs

make html

```

2) this will create the html output inside `./docs/build/html/`

3) open the index.html file to browse and see the results
