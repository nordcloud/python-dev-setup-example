PHONY: setup-dev

setup-dev:
	pipenv sync -d
	-git config --unset-all core.hooksPath
	pipenv run pre-commit install
